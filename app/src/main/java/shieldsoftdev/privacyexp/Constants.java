package shieldsoftdev.privacyexp;

/**
 * Created by DEJANK on 29.9.2016..
 */

public class Constants {
    public interface ACTION {
        public static String MAIN_ACTION = "shieldsoftdev.privacyexp.action.main";
        public static String START_MIC_BLOCK_ACTION = "shieldsoftdev.privacyexp.action.startmicblocking";
        public static String STOP_MIC_BLOCK_ACTION = "shieldsoftdev.privacyexp.action.stopmicblocking";
        public static String START_CAMERA_BLOCK_ACTION = "shieldsoftdev.privacyexp.action.startcamblocking";
        public static String STOP_CAMERA_BLOCK_ACTION = "shieldsoftdev.privacyexp.action.stopcamblocking";

    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}