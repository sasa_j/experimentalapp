package shieldsoftdev.privacyexp;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
    private LocationManager locationManager;

    private final int MY_PERMISSIONS_REQUEST = 20;
    private static final int REQUEST_CODE_ENABLE_DEVICE_ADMIN = 1;
    private static final int REQUEST_LOCATION_SETTINGS = 2;

    private Button grantAdminButton;
    private Button toggleCameraButton;
    private Button toggleLocationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        Button off = (Button) findViewById(R.id.off);
        Button on = (Button) findViewById(R.id.on);

        grantAdminButton = (Button) findViewById(R.id.grant_admin_button);
        toggleCameraButton = (Button) findViewById(R.id.toggle_camera_button);

        toggleLocationButton = (Button) findViewById(R.id.toggle_location_button);

        off.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (checkPermissions()) {
                    Intent startIntent = new Intent(MainActivity.this, MicBlockingService.class);
                    startIntent.setAction(Constants.ACTION.START_MIC_BLOCK_ACTION);
                    startService(startIntent);
                }else {
                    requestPermissions();
                }

            }
        });

        on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermissions()) {
                    Intent stopIntent = new Intent(MainActivity.this, MicBlockingService.class);
                    stopIntent.setAction(Constants.ACTION.STOP_MIC_BLOCK_ACTION);
                    startService(stopIntent);
                }
            }
        });


        grantAdminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestAdmin();
            }
        });

        toggleCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleCamera();
            }
        });


        toggleLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_LOCATION_SETTINGS);
            }
        });

        refreshState();
    }

    private Boolean checkPermissions() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    private void requestPermissions(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }


    private void toggleCamera() {
        DevicePolicyManager policyManager =
                (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!policyManager.isAdminActive(componentName())) {
            Toast.makeText(this, "Need to allow permissions first.", Toast.LENGTH_LONG).show();
            refreshState();
            return;
        }
        if(policyManager.isAdminActive(componentName()) && !policyManager.getCameraDisabled(componentName())){
            policyManager.setCameraDisabled(componentName(), true);
            Toast.makeText(this, "Camera Disabled", Toast.LENGTH_SHORT).show();

            startBlockingService(true);

            refreshState();
        }else if(policyManager.isAdminActive(componentName()) && policyManager.getCameraDisabled(componentName())){
            policyManager.setCameraDisabled(componentName(), false);
            Toast.makeText(this, "Camera Enabled", Toast.LENGTH_SHORT).show();

            startBlockingService(false);

            refreshState();
        }

    }


    private void requestAdmin() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName());
        startActivityForResult(intent, REQUEST_CODE_ENABLE_DEVICE_ADMIN);
        refreshState();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // After finishing the device admin activity, we get a callback and can refresh the state
        // then.
        refreshState();
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * These are the four stages of disabling the camera permanently, and each one corresponds to a
     * different UI "screen".
     */
    enum State {
        NEEDS_PERMISSIONS,
        CAMERA_DISABLED,
        CAMERA_ENABLED,
        IS_DEVICE_OWNER,
    }

    private void refreshState() {

        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            toggleLocationButton.setText("Enable GPS");
        }else{
            toggleLocationButton.setText("Disable GPS");
        }

        switch (getCurrentState()) {
            case NEEDS_PERMISSIONS:
                grantAdminButton.setEnabled(true);
                toggleCameraButton.setEnabled(false);
                break;
            case CAMERA_ENABLED:
                grantAdminButton.setEnabled(false);
                toggleCameraButton.setEnabled(true);
                toggleCameraButton.setText("Disable camera");
                break;
            case CAMERA_DISABLED:
                grantAdminButton.setEnabled(false);
                toggleCameraButton.setEnabled(true);
                toggleCameraButton.setText("Enable camera");
                break;
            case IS_DEVICE_OWNER:
                grantAdminButton.setEnabled(false);
                toggleCameraButton.setEnabled(true);
                break;
        }
    }

    private ComponentName componentName() {
        return new ComponentName(getApplicationContext(), AdminReceiver.class);
    }

    private State getCurrentState() {
        ComponentName componentName = componentName();
        DevicePolicyManager policyManager =
                (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!policyManager.isAdminActive(componentName)) {
            return State.NEEDS_PERMISSIONS;
        }
        if (policyManager.isAdminActive(componentName) && !policyManager.getCameraDisabled(componentName)) {
            return State.CAMERA_ENABLED;
        }
        if (policyManager.isAdminActive(componentName) && policyManager.getCameraDisabled(componentName)) {
            return State.CAMERA_DISABLED;
        }
//        if (!policyManager.isDeviceOwnerApp(BuildConfig.APPLICATION_ID)) {
//            return State.CAMERA_DISABLED;
//        }
        // Apparently we can't query for user restrictions, so just always set them if they're not
        // already set. We know that we're allowed to because we're the device owner.
//        policyManager.addUserRestriction(componentName, UserManager.DISALLOW_ADD_USER);
//        policyManager.addUserRestriction(componentName, UserManager.DISALLOW_FACTORY_RESET);
        return State.IS_DEVICE_OWNER;
    }

    private void startBlockingService(boolean start){
        if(start){
            Intent startIntent = new Intent(MainActivity.this, MicBlockingService.class);
            startIntent.setAction(Constants.ACTION.START_CAMERA_BLOCK_ACTION);
            startService(startIntent);
        }else{
            Intent startIntent = new Intent(MainActivity.this, MicBlockingService.class);
            startIntent.setAction(Constants.ACTION.STOP_CAMERA_BLOCK_ACTION);
            startService(startIntent);
        }
    }

}


