package shieldsoftdev.privacyexp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by DEJANK on 29.9.2016..
 */

public class MicBlockingService extends Service {


    private static final String TAG = MicBlockingService.class.getSimpleName();
    private MediaRecorder recorder = null;

    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format
    private static int[] mSampleRates = new int[] { 8000, 11025, 22050, 44100 };



    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if (intent.getAction().equals(Constants.ACTION.START_MIC_BLOCK_ACTION)) {
            Log.i(TAG, "Received Start Blocking Intent ");

//            //make a pipe containing a read and a write parcelfd
//            ParcelFileDescriptor[] fdPair = new ParcelFileDescriptor[0];
//            try {
//                fdPair = ParcelFileDescriptor.createPipe();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            //get a handle to your read and write fd objects.
//            ParcelFileDescriptor readFD = fdPair[0];
//            ParcelFileDescriptor writeFD = fdPair[1];

            recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setOutputFile(Environment.getExternalStorageDirectory().getAbsolutePath()+"/audiorecordtest.3gp");
            try {
                recorder.prepare();
                recorder.start();
                Toast.makeText(MicBlockingService.this, "Blocking mic: ON", Toast.LENGTH_SHORT).show();
                startNotification();
            } catch (IOException e) {
                e.printStackTrace();
            }
              // Recording is now started



        } else if (intent.getAction().equals(
                Constants.ACTION.STOP_MIC_BLOCK_ACTION)) {

            if (recorder != null) {
                recorder.stop();
                recorder.release();
                recorder = null;
                Toast.makeText(MicBlockingService.this, "Blocking mic: OFF", Toast.LENGTH_SHORT).show();
            }
            Log.i(TAG, "Received Stop Blocking Intent");
            stopForeground(true);
            stopSelf();

        } else if (intent.getAction().equals(Constants.ACTION.START_CAMERA_BLOCK_ACTION)){
            startNotification();

        } else if (intent.getAction().equals(Constants.ACTION.STOP_CAMERA_BLOCK_ACTION)){
            Log.i(TAG, "Received Stop Blocking Intent");
            stopForeground(true);
            stopSelf();
        }

        return START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "In onDestroy");
    }

    private void startNotification(){


        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);


        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Privacy exp")
                .setTicker("Privacy exp")
                .setContentText("Camera and microphone blocking")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();

        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);
    }

}
